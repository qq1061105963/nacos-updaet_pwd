package com.nacos.password;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 生成nacos登录密码
 *
 * @author 张国林
 * @date 2020-06-30 23:02
 */
public class UpdatePassword {

  public static void main(String[] args) {

    String password = "docker";

    System.out.println(new BCryptPasswordEncoder().encode(password));

  }

}
